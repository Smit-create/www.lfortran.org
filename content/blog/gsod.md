---
title: LFortran Google Season of Docs (GSoD)
date: 2022-05-16
tags: ["LFortran", "GSoD"]
author: "[Ondřej Čertík](https://ondrejcertik.com/), Rohit Goswami, Tapasweni Pathak"
type: post
---

LFortran has evolved rapidly since its inception and is now close to beta
status. Several key project progress goals are on track with the help of
external support; and the recent [GSoD support] will galvanize the documentation
and ease the onboarding of new contributors to the language and LFortran's
unique tooling.

At the heart of the L-family of compilers (LFortran, LPython)
lies the idea that there are invariants across languages which can be
documented and worked with. To this end, LFortran has decided to kick-start a
sustained period of documentation generation for both the Fortran language (in
terms of compiler implementation records) and the ASR itself. The relatively
higher technical debt required for the task at the moment made it inefficient
to look too far away from the existing community, and Rohit Goswami and
Tapasweni Pathak will be the two technical writers for this 2022 season of
documentation. You can look at our GSoD [proposal] for more details.

If you want to get involved or provide suggestions to our work, please do not
hesitate to do that. You can contact us at our LFortran [Zulip channel].


[GSoD support]: https://opensource.googleblog.com/2022/04/season-of-docs-announces-participating-organizations-for-2022.html
[proposal]: https://gitlab.com/lfortran/lfortran/-/wikis/GSoD%20Proposal%202022%20-%20LFortran%20Compiler%20Developer%20Documentation
[Zulip channel]: https://lfortran.zulipchat.com/
