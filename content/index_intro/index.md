---
headless: true
date: 2021-04-17
---

LFortran is a modern open-source (BSD licensed) interactive Fortran compiler
built on top of LLVM. It can execute user's code interactively to allow
exploratory work (much like Python, MATLAB or Julia) as well as compile to
binaries with the goal to run user's code on modern architectures such as
multi-core CPUs and GPUs.

LFortran is in alpha (it is expected to not work on third-party codes and users
enthusiastically participate in bug reporting and fixing). Progress towards
beta (it is expected to work on third-party codes, but there might still be bugs):

![Progress bar](/images/progress_bar.png)

LFortran will reach beta when it can reliably compile 10 third-party production codes. Current status:

* [Legacy Minpack](https://github.com/scipy/scipy/tree/f797ac7721310c7bd98bae416be1bed9975b4203/scipy/optimize/minpack) (February, 2023) and [Modern Minpack](https://github.com/fortran-lang/minpack) (May, 2023): [LFortran Breakthrough: Now Building Legacy and Modern Minpack](/blog/2023/05/lfortran-breakthrough-now-building-legacy-and-modern-minpack/)
* [fastGPT](https://github.com/certik/fastGPT) (September, 2023): [LFortran Compiles fastGPT](/blog/2023/09/lfortran-compiles-fastgpt/)

Main repository at GitHub:
[https://github.com/lfortran/lfortran](https://github.com/lfortran/lfortran)
{{< github_lfortran_button >}}

Try LFortran in your browser using WebAssembly: https://dev.lfortran.org/

Twitter: [@lfortranorg](https://twitter.com/lfortranorg)\
Any questions? Ask us on Zulip [![project chat](https://img.shields.io/badge/zulip-join_chat-brightgreen.svg)](https://lfortran.zulipchat.com/)
or our [mailing list](https://groups.io/g/lfortran). You can also use the
Fortran Discourse [forum](https://fortran-lang.discourse.group).
